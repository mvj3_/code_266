#import "sys/utsname.h"
/*!
*  get the information of the device and system
*  "i386"          simulator
*  "iPod1,1"       iPod Touch
*  "iPhone1,1"     iPhone
*  "iPhone1,2"     iPhone 3G
*  "iPhone2,1"     iPhone 3GS
*  "iPad1,1"       iPad
*  "iPhone3,1"     iPhone 4
*  @return null
*/
- (void)getDeviceAndOSInfo
{
//here use sys/utsname.h
struct utsname systemInfo;
uname(&systemInfo);
//get the device model and the system version
NSLog(@"%@", [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding]);
NSLog(@"%@", [[UIDevice currentDevice] systemVersion]);
}
